# coding=utf-8

class PersonagemNaoEncontradoException(Exception):
    def __init__(self, nome):
        message = 'Personagem {} não encontrado'.format(nome)
        super(PersonagemNaoEncontradoException, self).__init__(message)

