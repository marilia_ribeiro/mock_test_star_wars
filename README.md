## Getting Started - mock testes

Seguindo o tutorial de implementação de testes com mock. Disponível em: https://medium.com/meus-pedidos/testes-unit%C3%A1rios-e-python-parte-ii-mocks-apis-star-wars-ou-como-consumir-uma-api-rest-3a24d153db59

## Instalando dependências
- Crie uma virtualenv com python3 utilizando o seguinte comando: `python3 -m venv star_wars`
- Ative a virtualenv `star_wars`: `. star_wars/bin/activate`

- Instale as dependências contidas no arquivo requirements.txt: `pip install -r requirements.txt`

## Executando os testes:
Abaixo estão listadas duas formas de executar os testes.
- pytest: `pytest test_starwars_gateway_swapi.py`
- pytest-watch: `pytest-watch test_starwars_gateway_swapi.py`

## Leia mais sobre mock
- https://docs.python.org/3/library/unittest.mock.html
- https://docs.python.org/3/library/unittest.mock-examples.html

## Leia mais sobre unittest
- https://docs.python.org/3/library/unittest.html

# Leia mais sobre pytest
- https://docs.pytest.org/en/latest/
