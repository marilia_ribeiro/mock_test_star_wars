# coding=utf-8
from urllib import urlencode
from mock_tests_star_wars.exceptions import PersonagemNaoEncontradoException


class StarWarsGatewaySWAPI(object):
    URL = 'http://swapi.co/api/'

    def __init__(self, client_http):
        self.client = client_http

    def buscar_dados(self):
        return self.client.get(self.URL)

    def buscar_personagem(self, nome):
        recurso = 'people/?'
        parametros_de_busca = urlencode({'search': nome})

        url_completa = self.URL + recurso + parametros_de_busca

        response = self.client.get(url_completa)
        counteudo = response.json()

        if counteudo['count'] == 0:
            raise PersonagemNaoEncontradoException(nome)

        return response
