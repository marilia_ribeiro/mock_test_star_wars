# coding=utf-8
import unittest
import requests
# import mock
from unittest.mock import Mock
from mock_tests_star_wars.starwars_gateway_swapi import StarWarsGatewaySWAPI
from mock_tests_star_wars.exceptions import PersonagemNaoEncontradoException

class StarWarsGatewaySWAPITests(unittest.TestCase):
    def setUp(self):
        self.client = Mock(spec=requests) # 1 - criando ojeto Mock que simulará o HTTP - tem apenas os atributos e métodos que iremos chamar
        self.dados_star_wars_gateway = StarWarsGatewaySWAPI(self.client)

    def test_obtem_resposta_do_servidor(self):
        self.client.get.return_value = Mock(status_code=200) # 2 ciando um objeto Mock que será uma response com status_code que será retornado na chamada get do Mock

        resposta = self.dados_star_wars_gateway.buscar_dados()

        self.assertEqual(200, resposta.status_code)

    def test_obtem_personagem(self):
        retorno_json = { # 3 - dicinario que será utilizado
            'count': 1,
            'results': [
                {'name': 'Anakin Skywalker'}
            ]
        }

        # 4 - criando uma variável mock_response que irá retornar um método json
        mock_response = Mock(status_code=200) # 4 -
        mock_response.json.return_value = retorno_json
        self.client.get.return_value = mock_response

        resposta = self.dados_star_wars_gateway.buscar_personagem(nome='Anakin')

        conteudo = resposta.json()
        self.assertEqual('Anakin Skywalker', conteudo['results'][0]['name'])
        self.client.get.assert_called_with('http://swapi.co/api/people/?search=Anakin') # 5 - assert_called_with diz que o método foi chamdo com o argumento correto



    def test_lanca_excecao_quando_personagem_nao_existe(self):
        retorno_json = {
            'count': 0,
            'results': []
        }
        mock_response = Mock(status_code=200)
        mock_response.json.return_value = retorno_json
        self.client.get.return_value = mock_response

        with self.assertRaises(PersonagemNaoEncontradoException) as context:
            self.dados_star_wars_gateway.buscar_personagem(nome='Spock')

        self.assertEqual('Personagem Spock não encontrado', context.exception.message)

